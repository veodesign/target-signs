<?php

	require_once(TOOLKIT . '/class.event.php');

	Class eventmessage extends SectionEvent{

		public $ROOTELEMENT = 'message';

		public $eParamFILTERS = array(
			'etm-enqiury'
		);

		public static function about(){
			return array(
				'name' => 'Message',
				'author' => array(
					'name' => 'Rob Anderson',
					'website' => 'http://localhost/target-signs/public_html',
					'email' => 'robster31@googlemail.com'),
				'version' => 'Symphony 2.4',
				'release-date' => '2014-08-03T15:00:15+00:00',
				'trigger-condition' => 'action[message]'
			);
		}

		public static function getSource(){
			return '11';
		}

		public static function allowEditorToParse(){
			return true;
		}

		public static function documentation(){
			return '
                <h3>Success and Failure XML Examples</h3>
                <p>When saved successfully, the following XML will be returned:</p>
                <pre class="XML"><code>&lt;message result="success" type="create | edit">
    &lt;message>Entry [created | edited] successfully.&lt;/message>
&lt;/message></code></pre>
                <p>When an error occurs during saving, due to either missing or invalid fields, the following XML will be returned:</p>
                <pre class="XML"><code>&lt;message result="error">
    &lt;message>Entry encountered errors when saving.&lt;/message>
    &lt;field-name type="invalid | missing" />
...&lt;/message></code></pre>
                <p>The following is an example of what is returned if any options return an error:</p>
                <pre class="XML"><code>&lt;message result="error">
    &lt;message>Entry encountered errors when saving.&lt;/message>
    &lt;filter name="admin-only" status="failed" />
    &lt;filter name="send-email" status="failed">Recipient not found&lt;/filter>
...&lt;/message></code></pre>
                <h3>Example Front-end Form Markup</h3>
                <p>This is an example of the form markup you can use on your frontend:</p>
                <pre class="XML"><code>&lt;form method="post" action="{$current-url}" enctype="multipart/form-data">
    &lt;input name="MAX_FILE_SIZE" type="hidden" value="5242880" />
    &lt;label>Name
        &lt;input name="fields[name]" type="text" />
    &lt;/label>
    &lt;label>Email
        &lt;input name="fields[email]" type="text" />
    &lt;/label>
    &lt;label>Phone
        &lt;input name="fields[phone]" type="text" />
    &lt;/label>
    &lt;label>Enquiry
        &lt;textarea name="fields[enquiry]" rows="15" cols="50">&lt;/textarea>
    &lt;/label>
    &lt;input name="action[message]" type="submit" value="Submit" />
&lt;/form></code></pre>
                <p>To edit an existing entry, include the entry ID value of the entry in the form. This is best as a hidden field like so:</p>
                <pre class="XML"><code>&lt;input name="id" type="hidden" value="23" /></code></pre>
                <p>To redirect to a different location upon a successful save, include the redirect location in the form. This is best as a hidden field like so, where the value is the URL to redirect to:</p>
                <pre class="XML"><code>&lt;input name="redirect" type="hidden" value="http://localhost/target-signs/public_html/success/" /></code></pre>';
		}

		public function load(){
			if(isset($_POST['action']['message'])) return $this->__trigger();
		}

	}
