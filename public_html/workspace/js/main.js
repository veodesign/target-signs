/*!
 * Filename: main.js
 * Bootstrap code for the JS
*/

define(function(){
	"use strict";
	
	var app = {};
	app.load = function(url,production){
		
		if(typeof production === 'undefined'){
			production = false;
		}
		
		/**
			Allow custom URL injection
		*/
		if(url !== null){
			require.config({
				baseUrl: url+'/workspace/js'
			});
		}

		if(production){
			require(['../app.min'], function(){
				require(['global-config'], function(){
					require(['app'],function(App){

						//Init the App
						App.start();
					});
				});
			});
		}
		else{
			
			require(['global-config'], function(){
				require(['app'],function(App){
					App.start();
				});
			});
		}
	};
	
	/**
	
		Determine if production or not
		
	*/
	app.load(null, document.getElementById('production-mode').value === 'Yes');
	
	return app;
});



