/* app.js
 * Javascript Application for LSR Holidays
 *
 * David Anderson 2013
 *
*/

define([
	'jquery', 'underscore','backbone','marionette','app/router',
	'bootstrap', 'colorbox'
], function($,_,Backbone,Marionette,AppRouter,Bootstrap){
	"use strict";
	
	
	var App = new Backbone.Marionette.Application(
	/** @lends App */
	{
		//routes
		index: function(){
			
			$('.carousel').carousel({
				interval: 4000
  			});
		},
		
		contact: function() {
			
			
			var lat = $('#latitude').val();
			var lon = $('#longitude').val();
			var title = $('#settings-tittle').val();
			
			var position = new google.maps.LatLng(parseFloat(lat), parseFloat(lon));
			
			var mapOptions = {
				center: position,
				zoom: 14,
			};
			
			var map = new google.maps.Map(document.getElementById("map-widget"), mapOptions);
			
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				title: title,
			});
		},
		
		
		services: function() {
			
			$('.gallery-image').colorbox({rel:'services'});
			
			$('.show-more a').click(function(e) {
				
				e.preventDefault();

				var data = $(e.currentTarget).data("gallery");
				
				var selector = '.gallery-section-extra[data-gallery="' + data + '"]';
				
				var item = $(selector)[0];
				
				if (item.style.display === 'block') {
					
					item.style.display = 'none';
					e.currentTarget.text = 'Show More';
				}
				else {
					
					item.style.display = 'block';
					e.currentTarget.text = 'Hide';
				}
			})
		},
		
		products: function() {
			
			$('.gallery-image').colorbox({rel:'products'});
		},
		
		projects: function() {
			
			$('.gallery-image').colorbox({rel:'projects'});
		},
	});
	
	/*
		
		Init Router
		
	*/
	App.addInitializer(function(options){
		options = options || {};
		
		this.router = new AppRouter({
			controller: App
		});
		if( ! Backbone.History.started){
			var base = $('base').attr('href').replace(window.location.origin,"");
			Backbone.history.start({pushState: true,root: base});
		}

	});
	
	return App;
});
