require.config({
	paths: {
		jquery: 'libs/jquery',
		'jquery-ui': 'libs/jquery-ui',
		underscore: 'libs/underscore',
		backbone: 'libs/backbone',
		marionette: 'libs/backbone.marionette',
		bootstrap: 'libs/bootstrap',
		moment: 'libs/moment',
		colorbox: "../colorbox/jquery.colorbox"
	},
		
	shim: {
		modernizr: {exports: 'Modernizr'},
		jquery: {exports: '$'},
		underscore: {exports: '_'},
		backbone: {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		marionette: {
			deps: ['underscore','jquery','backbone']
		},
		'jquery-ui': {deps: ['jquery']},
		bootstrap: {deps: ['jquery']},
		colorbox: {deps: ['jquery']}
	}
});
