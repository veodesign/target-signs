/*!
 * Filename: main.js
 * Bootstrap code for the JS
*/

define([
	'underscore','jquery','backbone','marionette'
],function(_,$,Backbone,Marionette){
	"use strict";
	
	var Router = Backbone.Marionette.AppRouter.extend({

		// "someMethod" must exist at controller.someMethod
		appRoutes: {
			"": "index",
			"contact/": "contact",
			"services/": "services",
			"products/": "services",
			"projects/": "services",
			"products/p/:handle/:id/": "services",
			"services/s/:handle/:id/": "services",
		},
	
		/* standard routes can be mixed with appRoutes/Controllers above */
		routes : {}
	});
	
	return Router;

});



