<?php

Class EnqiuryEmailTemplate extends EmailTemplate{


	public $datasources = Array(
 			'contact',
 			'messages_by_id',
 			'settings',);
	public $layouts = Array(
 			'html' => 'template.html.xsl',);
	public $subject = 'New Website Enquiry';
	public $reply_to_name = '';
	public $reply_to_email_address = '';
	public $recipients = '{//data/settings/entry/email-address}';

	public $editable = true;

	public $about = Array(
		'name' => 'Enqiury',
		'version' => '1.0',
		'author' => array(
			'name' => 'Rob Anderson',
			'website' => 'http://localhost/target-signs/public_html',
			'email' => 'robster31@googlemail.com'
		),
		'release-date' => '2014-08-03T14:40:36+00:00'
	);
}