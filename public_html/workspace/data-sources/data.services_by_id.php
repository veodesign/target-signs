<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourceservices_by_id extends SectionDatasource {

		public $dsParamROOTELEMENT = 'services-by-id';
		public $dsParamORDER = 'desc';
		public $dsParamPAGINATERESULTS = 'yes';
		public $dsParamLIMIT = '20';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'yes';
		public $dsParamREQUIREDPARAM = '$serv-id';
		public $dsParamSORT = 'system:id';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';

		public $dsParamFILTERS = array(
				'system:id' => '{$serv-id}',
		);

		public $dsParamINCLUDEDELEMENTS = array(
				'system:pagination',
				'system:date',
				'main-content',
				'title',
				'content: formatted',
				'image: title',
				'image: image',
				'image: description',
				'order',
				'gallery-images: title',
				'gallery-images: image',
				'gallery-images: description',
				'search-engine-optimisation',
				'description',
				'keywords'
		);

		public function __construct($env=NULL, $process_params=true) {
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about() {
			return array(
				'name' => 'Services: by id',
				'author' => array(
					'name' => 'Rob Anderson',
					'website' => 'http://localhost/target-signs/public_html',
					'email' => 'robster31@gmail.com'),
				'version' => 'Symphony 2.4',
				'release-date' => '2015-09-08T10:30:43+00:00'
			);
		}

		public function getSource() {
			return '9';
		}

		public function allowEditorToParse() {
			return true;
		}

		public function execute(array &$param_pool = null) {
			$result = new XMLElement($this->dsParamROOTELEMENT);

			try{
				$result = parent::execute($param_pool);
			}
			catch(FrontendPageNotFoundException $e){
				// Work around. This ensures the 404 page is displayed and
				// is not picked up by the default catch() statement below
				FrontendPageNotFoundExceptionHandler::render($e);
			}
			catch(Exception $e){
				$result->appendChild(new XMLElement('error', $e->getMessage() . ' on ' . $e->getLine() . ' of file ' . $e->getFile()));
				return $result;
			}

			if($this->_force_empty_result) $result = $this->emptyXMLSet();

			if($this->_negate_result) $result = $this->negateXMLSet();

			return $result;
		}

	}