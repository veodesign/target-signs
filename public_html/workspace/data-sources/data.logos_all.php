<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourcelogos_all extends SectionDatasource {

		public $dsParamROOTELEMENT = 'logos-all';
		public $dsParamORDER = 'desc';
		public $dsParamPAGINATERESULTS = 'no';
		public $dsParamLIMIT = '20';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'no';
		public $dsParamSORT = 'order';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';

		public $dsParamINCLUDEDELEMENTS = array(
				'title',
				'icon: title',
				'icon: image'
		);

		public function __construct($env=NULL, $process_params=true) {
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about() {
			return array(
				'name' => 'Logos: All',
				'author' => array(
					'name' => 'Rob Anderson',
					'website' => 'http://localhost/target-signs/public_html',
					'email' => 'robster31@googlemail.com'),
				'version' => 'Symphony 2.4',
				'release-date' => '2014-08-06T22:39:54+00:00'
			);
		}

		public function getSource() {
			return '13';
		}

		public function allowEditorToParse() {
			return true;
		}

		public function execute(array &$param_pool = null) {
			$result = new XMLElement($this->dsParamROOTELEMENT);

			try{
				$result = parent::execute($param_pool);
			}
			catch(FrontendPageNotFoundException $e){
				// Work around. This ensures the 404 page is displayed and
				// is not picked up by the default catch() statement below
				FrontendPageNotFoundExceptionHandler::render($e);
			}
			catch(Exception $e){
				$result->appendChild(new XMLElement('error', $e->getMessage() . ' on ' . $e->getLine() . ' of file ' . $e->getFile()));
				return $result;
			}

			if($this->_force_empty_result) $result = $this->emptyXMLSet();

			if($this->_negate_result) $result = $this->negateXMLSet();

			return $result;
		}

	}