<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<!-- ********************************* -->
<!-- page init -->
<!-- ********************************* -->

<!--<xsl:template match="data" mode="init-js"></xsl:template>-->
<!--<xsl:template match="data" mode="site-title"></xsl:template>-->



<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->
<xsl:include href="../utilities/master.xsl" />





<!-- ********************************* -->
<!-- Site Meta -->
<!-- ********************************* -->
<xsl:template match="data" mode="site-title">		<xsl:value-of select="$seo/home-page-title" />			</xsl:template>
<xsl:template match="data" mode="site-description">		<xsl:value-of select="$seo/home-page-description" />	</xsl:template>




<!-- ********************************* -->
<!-- data root -->
<!-- ********************************* -->
<xsl:template match="/data">
	
	
	
	<xsl:call-template name="carousel"/>
	
	
	
	
	<!-- Description -->
	<div class="light-bar">
		<div id="home-content">
			<div class="row">
				<div class="col-md-8">
					<div id="description-text">
						<h2> <xsl:value-of select="$home/title"/> </h2>
						<p class="quote"> <xsl:value-of select="$home/quote"/> </p>
						<xsl:copy-of select="$home/content/node()"/>
					</div>
				</div>
				
				<xsl:call-template name="contact-form"/>
			</div>
		</div>

	</div>
	
	
	
	
	<div class="white-bar clearfix">
		<div class="row">
			<div class="col-md-12">
				<xsl:call-template name="logos"/>
			</div>
		</div>
	</div>
	
	
</xsl:template>


<!-- ********************************* -->
<!-- template positions -->
<!-- ********************************* -->


</xsl:stylesheet>