<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:output method="xml"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />
	

<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->
<xsl:include href="../utilities/master.xsl" />



<xsl:template match="/data">
	
	<div class="white-bar container" >
		<div class="col-md-12">
			<xsl:call-template name="testimonies">
				<xsl:with-param name="max-items" select="1000"/>
			</xsl:call-template>
		</div>
	</div>
	
</xsl:template>

</xsl:stylesheet>