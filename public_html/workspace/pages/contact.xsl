<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	
<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->
<xsl:include href="../utilities/master.xsl" />



<!-- ********************************* -->
<!-- Site Meta -->
<!-- ********************************* -->
<xsl:template match="data" mode="site-title">		<xsl:value-of select="$seo/contact-page-title" />			</xsl:template>
<xsl:template match="data" mode="site-description">		<xsl:value-of select="$seo/contact-page-description" />	</xsl:template>






<xsl:template match="/data">
	
	
	<!-- Map Widget -->
	<div id="map-widget" class="light-bar">  </div>
	
	
	<!-- Content -->
	<div class="light-bar">
		<div class="row">
		
			<!-- Details -->
			<div class="col-md-8">
				<div class="contact-info">
					<p> <xsl:copy-of select="$contact/content/node()"/> </p>
				</div>
			</div>
			
			<!-- Contact Us -->
			<xsl:call-template name="contact-form"/>
			
		</div>
	</div>
	
	
	
	<!-- Hidden Values -->
	<input type="hidden" value="{$contact/latitude}" id="latitude"/>
	<input type="hidden" value="{$contact/longitude}" id="longitude"/>
	<input type="hidden" value="{$settings/site-title}" id="settings-title"/>
	
	
</xsl:template>




</xsl:stylesheet>