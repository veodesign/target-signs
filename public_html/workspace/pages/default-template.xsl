<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<!-- ********************************* -->
<!-- page init -->
<!-- ********************************* -->

<!--<xsl:template match="data" mode="init-js"></xsl:template>-->
<!--<xsl:template match="data" mode="site-title"></xsl:template>-->



<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->
<xsl:include href="../utilities/master.xsl" />
<xsl:include href="../utilities/form_builder.xsl" />

<!-- ********************************* -->
<!-- data root -->
<!-- ********************************* -->
<xsl:template match="/data">
	
</xsl:template>


<!-- ********************************* -->
<!-- template positions -->
<!-- ********************************* -->


</xsl:stylesheet>