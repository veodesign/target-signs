<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />


<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->
<xsl:include href="../utilities/master.xsl" />



<xsl:template match="/data">
	
	
	<div class="light-bar">
	
		<div class="container gallery">
			
			<xsl:for-each select="//data/gallery-images-all/entry">
				
				<div class="col-md-3">
					
					<div class="gallery-item">
					
						<a class="gallery-image" title="{image/item/title}" >
						
							<!-- The Link to the bigger Image -->
							<xsl:attribute name="href"> 
								<xsl:call-template name="image-path">
									<xsl:with-param name="mode" select="'full'" />
									<xsl:with-param name="file"  select="image/item/image/filename"/>
									<xsl:with-param name="path" select="image/item/image/@path"/>						
									<xsl:with-param name="width" select="960" />
									<xsl:with-param name="height" select="540" />
								</xsl:call-template>
							</xsl:attribute>
							
							
							<!-- The Icon Image -->
							<xsl:call-template name="image-fit">
								<xsl:with-param name="item" select="image/item/image"/>
								<xsl:with-param name="alt" select="Test"/>
								<xsl:with-param name="width" select="'255'"/>
								<xsl:with-param name="height" select="'150'"/>
							</xsl:call-template>
						</a>
					</div>
					
				</div>
				
			</xsl:for-each>
			
		</div>
	
	</div>
	
</xsl:template>

</xsl:stylesheet>