<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"
	doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
	omit-xml-declaration="yes"
	encoding="UTF-8"
	indent="yes" />


<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->
<xsl:include href="../utilities/master.xsl" />



<!-- ********************************* -->
<!-- Site Meta -->
<!-- ********************************* -->
<xsl:template match="data" mode="site-title">		<xsl:value-of select="$seo/products-page-title" />			</xsl:template>
<xsl:template match="data" mode="site-description">		<xsl:value-of select="$seo/products-page-description" />	</xsl:template>



<xsl:template match="/data">

	<xsl:call-template name="carousel"/>
	
	<xsl:call-template name="general-service">
		<xsl:with-param name="data" select="//data/products-all/entry"/>
		<xsl:with-param name="link" select="'products/p'"/>
	</xsl:call-template>
	
</xsl:template>

</xsl:stylesheet>