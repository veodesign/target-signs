<?xml version="1.0" encoding="UTF-8"?>
<!-- master.xsl
 * Master xsl file for Symphony template
 * Author: David Anderson 2011
 * dave@veodesign.co.uk
-->
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:date="http://exslt.org/dates-and-times"
		xmlns:exsl="http://exslt.org/common"
		xmlns:form="http://nick-dunn.co.uk/xslt/form-controls"
		extension-element-prefixes="exsl form date">

<!-- ********************************* -->
<!-- includes -->
<!-- ********************************* -->

<!-- symphony utils -->
<xsl:import href="typography.xsl"/>
<xsl:import href="html-truncate.xsl"/>
<xsl:import href="date-time.xsl"/>
<xsl:import href="form_builder.xsl"/>

<!--custom utils-->
<xsl:import href="string.xsl"/>
<xsl:import href="image-format.xsl"/>

<xsl:output method="xml"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    omit-xml-declaration="yes"
    encoding="UTF-8"
    indent="yes" />


<!-- ********************************* -->
<!-- global variables -->
<!-- ********************************* -->
<xsl:variable name="site-title" select="$settings/site-title" />
<xsl:variable name="settings" select="//data/settings/entry" />
<xsl:variable name="home" select="//data/home/entry" />
<xsl:variable name="contact" select="//data/contact/entry" />
<xsl:variable name="services" select="//data/services/entry" />
<xsl:variable name="form:event" select="//data/events/message" />
<xsl:variable name="seo" select="//data/seo-content/entry"/>
<!-- ********************************* -->
<!-- root template -->
<!-- ********************************* -->


<xsl:template match="/">

	<xsl:comment><![CDATA[[if lt IE 7 ]><html xmlns:fb="http://ogp.me/ns/fb#" lang="en" class="no-js ie6 ]]><xsl:value-of select="concat('page-',$current-page)"/><![CDATA["><![endif]]]></xsl:comment>
			<xsl:comment><![CDATA[[if IE 7 ]><html xmlns:fb="http://ogp.me/ns/fb#" lang="en" class="no-js ie7 ]]><xsl:value-of select="concat('page-',$current-page)"/><![CDATA["><![endif]]]></xsl:comment>
			<xsl:comment><![CDATA[[if IE 8 ]><html xmlns:fb="http://ogp.me/ns/fb#" lang="en" class="no-js ie8 ]]><xsl:value-of select="concat('page-',$current-page)"/><![CDATA["><![endif]]]></xsl:comment>
			<xsl:comment><![CDATA[[if IE 9 ]><html xmlns:fb="http://ogp.me/ns/fb#" lang="en" class="no-js ie9 ]]><xsl:value-of select="concat('page-',$current-page)"/><![CDATA["><![endif]]]></xsl:comment>
			<xsl:comment><![CDATA[[if (gt IE 9)|!(IE)]><!]]></xsl:comment><html lang="en" xmlns:fb="http://ogp.me/ns/fb#" class="page-{$current-page}"><xsl:comment><![CDATA[<![endif]]]></xsl:comment>
	<head>

		<!-- Basic Page Needs
	  	================================================== -->
	  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><xsl:apply-templates select="data"  mode="site-title"/></title>

		<base href="{$root}/" />

		<!-- Meta
	  	================================================== -->
		<meta name="robots" content="index, follow" />
		<meta name="description">
			<xsl:attribute name="content"><xsl:apply-templates select="data"  mode="site-description"/></xsl:attribute>
		</meta>
		<meta name="keywords">
			<xsl:attribute name="content"><xsl:apply-templates select="data"  mode="site-keywords"/></xsl:attribute>
		</meta>


		<meta name="google-site-verification" content="{$settings/google-site-verification}" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

		<!-- Favicons
		================================================== -->
		<link rel="shortcut icon" href="{$workspace}/images/favicon.ico"/>
		<link rel="apple-touch-icon" href="{$workspace}/images/apple-touch-icon.png"/>
		<link rel="apple-touch-icon" sizes="72x72" href="{$workspace}/images/apple-touch-icon-72x72.png"/>
		<link rel="apple-touch-icon" sizes="114x114" href="{$workspace}/images/apple-touch-icon-114x114.png"/>

	    <!-- Template CSS
		================================================== -->
		<link rel="stylesheet" href="{$workspace}/stylesheets/screen.css" media="screen, projection" type="text/css"/>
		<link rel="stylesheet" href="{$workspace}/stylesheets/print.css" media="print" type="text/css"/>


		<!-- Colorbox CSS
		================================================== -->
		<link rel="stylesheet" type="text/css" href="{$workspace}/colorbox/colorbox.css"/>

		<!--[if IE]>
			<link href="/stylesheets/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<![endif]-->

		<!--JS-->
		<!--JS-->

		<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGGVQtMSgWfkQNojvIeSNv81Kk4QWxsmU" type="text/javascript"></script>
		<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
		<script data-main="{$workspace}/js/main.js" src="{$workspace}/js/require.js"></script>


	</head>

	<body class="page-{$current-page}">
		<input id="production-mode" value="{$settings/production-mode}" type="hidden" />


		<!-- Icon & Contact info -->
		<div>
			<div id="info-main" class="container">


				<!-- Image -->
				<div class="col-md-6">
					<img src="workspace/images/main-icon.png" alt="Our icon"/>
				</div>


				<div class="col-md-1 slogan">
					<p class="no-mobile"> illuminated <br/> sign <br/> specialists </p>
				</div>


				<!-- Contact -->
				<div class="col-md-5">
					<div id="info-contact">
						<p id="contact-number" class="dark-text">
							<xsl:value-of select="$settings/phone-number"/>
						</p>
						<p id="contact-email" class="dark-text">
							<xsl:value-of select="$settings/email-address"/>
						</p>
					</div>
				</div>
			</div>
		</div>


		<!-- Menu bar -->
		<div class="orange-bar">
			<div id="menu-main" class="container">


				<!-- Menu -->
				<ul class="pull-right">
					<li><a href="{$root}/home"> Home </a></li>
					<li><a href="{$root}/products"> Products </a></li>
					<li><a href="{$root}/services"> Services </a></li>
					<li><a href="{$root}/projects"> Projects </a></li>
					<li><a href="{$root}/contact"> Contact </a></li>
				</ul>
			</div>
		</div>



		<!-- Main Content -->
		<div class="background-image">
			<div class="container">
				<div class="row">
					<div class="background-inner">
						<xsl:apply-templates select="data"/>
					</div>
				</div>
			</div>
		</div>



		<!-- Footer -->
		<div class="orange-bar">
			<div id="footer-main" class="container">
				<div class="col-md-12">
					<xsl:copy-of select="$settings/footer/node()"/>
				</div>
			</div>
		</div>

	</body>
	</html>
</xsl:template>


<xsl:template name="carousel">

	<!-- Slideshow -->
	<div class="dark-bar clearfix">


			<div class="carousel slide" data-ride="carousel">


				<!-- Indicators -->
				<ol class="carousel-indicators">
					<xsl:for-each select="slideshow/entry">
						<xsl:variable name="active-i"><xsl:if test="position() = 1">active</xsl:if></xsl:variable>
						<li data-target=".carousel" data-slide-to="{position()-1}" class="{$active-i}"></li>
					</xsl:for-each>
				</ol>


				<!-- Wrapper for slides -->
				<div class="carousel-inner">

					<xsl:for-each select="slideshow/entry">

						<xsl:variable name="active"><xsl:if test="position() = 1">active</xsl:if></xsl:variable>

					    <div class="item {$active}">

							<xsl:call-template name="img">
								<xsl:with-param name="src" select="image/item/image/filename"/>
								<xsl:with-param name="path" select="image/item/image/@path"/>
								<xsl:with-param name="width" select="'1180'" />
								<xsl:with-param name="height" select="'400'"/>
								<xsl:with-param name="alt" select="image/item/title"/>
								<xsl:with-param name="mode" select="'crop-fill'"/>
							</xsl:call-template>

							<div class="carousel-caption">
								<h3><xsl:value-of select="content"/></h3>
							</div>
						</div>
					</xsl:for-each>
				</div>


				<!-- Controls -->
				<a class="left carousel-control" href=".carousel" role="button" data-slide="prev">
			  		<span class="glyphicon glyphicon-chevron-left"></span>
			  		</a>
			  	<a class="right carousel-control" href=".carousel" role="button" data-slide="next">
			  		<span class="glyphicon glyphicon-chevron-right"></span>
			  	</a>
			</div>

	</div>

</xsl:template>


<xsl:template name="testimonies">

	<xsl:param name="max-items" select="2"/>

	<!-- Testimony -->
	<div class="white-bar">
		<div id="testimony-main" class="row">

			<xsl:for-each select="testimonials-all/entry[position()&lt;=$max-items]">

				<div class="col-md-6">
				<div class="testimony row">

					<!-- Icon -->
					<div class="testimony-icon col-md-2">

						<xsl:call-template name="img">
							<xsl:with-param name="src" select="image/item/image/filename"/>
							<xsl:with-param name="path" select="image/item/image/@path"/>
							<xsl:with-param name="width" select="'100'" />
							<xsl:with-param name="height" select="'75'"/>
							<xsl:with-param name="alt" select="image/item/title"/>
							<xsl:with-param name="mode" select="'crop-fill'"/>
						</xsl:call-template>

					</div>

					<!-- Citation -->
					<div class="testimony-content col-md-10">

						<div class="testimony-message">
							<xsl:copy-of select="content/node()"/>
						</div>

						<!-- Author -->
						<div class="testimony-author">
							<p class="testimony-author">  <xsl:value-of select="name"/> </p>
						</div>

					</div>
				</div>
			</div>

			</xsl:for-each>
		</div>
	</div>

</xsl:template>


<xsl:template name="logos">

	<!-- Testimony -->
	<div class="white-bar">
		<div class="row all-industry-logos">

			<xsl:for-each select="logos-all/entry">

				<div class="col-sm-2">
					<div class="industry-logo">

						<xsl:call-template name="img">
							<xsl:with-param name="src" select="icon/item/image/filename"/>
							<xsl:with-param name="path" select="icon/item/image/@path"/>
							<xsl:with-param name="width" select="'120'" />
							<xsl:with-param name="height" select="'90'"/>
							<xsl:with-param name="alt" select="icon/item/title"/>
							<xsl:with-param name="mode" select="'resize'"/>
						</xsl:call-template>

					</div>
				</div>

			</xsl:for-each>
		</div>
	</div>

</xsl:template>


<xsl:template name="contact-form">


	<div class="col-md-4">
		<div class="orange-bar">
			<div class="form contact-us">
				<xsl:choose>

					<xsl:when test="$form:event/@result='success'">
						<div class="success">
							<h1>Message Sent</h1>
							<p>We will get back to you as soon as possible</p>
						</div>
					</xsl:when>


					<xsl:otherwise>
						<form method="post" action="https://formspree.io/web-enquiries@targetsigns.co.uk" enctype="multipart/form-data" id="contact">

							<input name="MAX_FILE_SIZE" type="hidden" value="5242880" />
							<!--
							<input name="akismet[author]" value="name" type="hidden" />
							<input name="akismet[email]" value="email" type="hidden" />
							<input name="akismet[url]" value="website" type="hidden" />
							-->

							<h3 class="light-text">GET A QUOTE</h3>


							<!-- Name -->
							<div class="form-item clearfix {$form:event/name/@type}">
								<div class="form-r">
									<xsl:call-template name="form:input">
										<xsl:with-param name="placeholder" select="'Name'"/>
										<xsl:with-param name="handle" select="'name'"/>
										<xsl:with-param name="class" select="'text form-control'"/>
									</xsl:call-template>

									<xsl:if test="$form:event/name/@type">
										<div class="error"><xsl:value-of select="$form:event/name/@message"/></div>
									</xsl:if>
								</div>
							</div>


							<!-- Email -->
							<div class="form-item clearfix {$form:event/email/@type}">
								<div class="form-r">
									<xsl:call-template name="form:input">
										<xsl:with-param name="placeholder" select="'Email'"/>
										<xsl:with-param name="handle" select="'email'"/>
										<xsl:with-param name="class" select="'text form-control'"/>
									</xsl:call-template>

									<xsl:if test="$form:event/email/@type">
										<div class="error"><xsl:value-of select="$form:event/email/@message"/></div>
									</xsl:if>
								</div>
							</div>


							<!-- Phone -->
							<div class="form-item clearfix {$form:event/phone/@type}">
								<div class="form-r">
									<xsl:call-template name="form:input">
										<xsl:with-param name="placeholder" select="'Phone Number'"/>
										<xsl:with-param name="handle" select="'phone'"/>
										<xsl:with-param name="class" select="'text form-control'"/>
									</xsl:call-template>

									<xsl:if test="$form:event/phone/@type">
										<div class="error"><xsl:value-of select="$form:event/phone/@message"/></div>
									</xsl:if>
								</div>
							</div>


							<!-- Message -->
							<div class="form-item clearfix {$form:event/enquiry/@type}">
								<div class="form-r">
									<xsl:call-template name="form:textarea">
										<xsl:with-param name="placeholder" select="'Enquiry'"/>
										<xsl:with-param name="handle" select="'enquiry'"/>
										<xsl:with-param name="class" select="'textarea form-control'"/>
										<xsl:with-param name="rows" select="'10'"/>
										<xsl:with-param name="cols" select="'30'"/>
									</xsl:call-template>

									<xsl:if test="$form:event/enquiry/@type">
										<div class="error"><xsl:value-of select="$form:event/enquiry/@message"/></div>
									</xsl:if>
								</div>
							</div>

							<input type="hidden" name="fields[website]" value="" />

							<div class="form-submit clearfix">
								<input name="action[message]" type="submit" value="Submit"  class="submit-button form-control" />
							</div>

						</form>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</div>

</xsl:template>


<xsl:template name="general-service">

	<xsl:param name="data"/>
	<xsl:param name="link" select="''"/>

	<div class="light-bar">

		<div class="container">

			<xsl:for-each select="$data">

				<xsl:call-template name="general-item">
					<xsl:with-param name="data" select="."/>
					<xsl:with-param name="link" select="$link"/>
				</xsl:call-template>

			</xsl:for-each>

		</div>

	</div>

</xsl:template>

<xsl:template name="general-item">

	<xsl:param name="data"/>
	<xsl:param name="link" select="''"/>

	<div class="service-item short">

		<a href="{$link}/{$data/title/@handle}/{$data/@id}">

			<div class="row">


				<!-- The image -->
				<div class="col-md-3">
					<div class="service-image">

						<xsl:call-template name="img">
							<xsl:with-param name="src" select="$data/image/item/image/filename"/>
							<xsl:with-param name="path" select="$data/image/item/image/@path"/>
							<xsl:with-param name="width" select="'300'" />
							<xsl:with-param name="height" select="'225'"/>
							<xsl:with-param name="alt" select="$data/image/item/title"/>
							<xsl:with-param name="mode" select="'crop-fill'"/>
						</xsl:call-template>

					</div>
				</div>


				<!-- The content -->
				<div class="col-md-9">
					<div class="service-content">

						<!-- The Title -->
						<!-- <h2 class="service-title"> <xsl:value-of select="$data/title"/> </h2> -->

						<div class="service-description">
							<xsl:call-template name="truncate">
								<xsl:with-param name="node" select="$data/content"/>
								<xsl:with-param name="limit" select="'300'"/>
							</xsl:call-template>
						</div>

					</div>
				</div>
			</div>

		</a>

	</div>


</xsl:template>


<xsl:template name="general-item-detail">

	<xsl:param name="data" />

	<div class="light-bar clearfix">

		<div class="service-item row">

			<div class="service-image col-md-3">
				<xsl:call-template name="img">
					<xsl:with-param name="src" select="$data/image/item/image/filename"/>
					<xsl:with-param name="path" select="$data/image/item/image/@path"/>
					<xsl:with-param name="width" select="'300'" />
					<xsl:with-param name="height" select="'225'"/>
					<xsl:with-param name="alt" select="$data/image/item/title"/>
					<xsl:with-param name="mode" select="'crop-fill'"/>
				</xsl:call-template>
			</div>


			<div class="service-content col-md-9">


				<!-- Title
				<h2 class="service-title">
					<xsl:value-of select="$data/title"/>
				</h2>
				-->


				<!-- Description -->
				<div class="service-description">
					<xsl:copy-of select="$data/content/node()"/>
				</div>


				<!-- Gallery -->
				<div class="service-gallery row">

					<xsl:variable name="service" select="$data/@id"/>


					<!-- Do the first 6 in one row -->
					<div class="row gallery-section">

						<xsl:for-each select="$data/gallery-images/item[not(position() &gt; 6)]">

							<xsl:call-template name="general-icon">
								<xsl:with-param name="image" select="image"/>
							</xsl:call-template>

						</xsl:for-each>

					</div>


					<!-- Do the next 6 in a show / hide div -->
					<div class="row gallery-section gallery-section-extra" data-gallery="gallery-{position()}">

						<xsl:for-each select="$data/gallery-images/item[not(position() &lt; 7)]">

							<xsl:call-template name="general-icon">
								<xsl:with-param name="image" select="image"/>
							</xsl:call-template>

						</xsl:for-each>

					</div>

					<!-- A show / hide button for the extra images -->
					<div class="row">
						<xsl:if test="count($data/gallery-images/item) &gt; 6 ">
							<div class="show-more">
								<a href="" data-gallery="gallery-{position()}"> Show More </a>
							</div>
						</xsl:if>
					</div>

				</div>

			</div>

		</div>

	</div>
</xsl:template>


<xsl:template name="general-icon">

	<xsl:param name="image" />

	<div class="col-md-2">

		<div class="gallery-item">

			<a class="gallery-image" title="{$image/description}" >

				<!-- The Link to the bigger Image -->
				<xsl:attribute name="href">
					<xsl:call-template name="image-fit-path">
						<xsl:with-param name="item" select="$image" />
						<xsl:with-param name="width" select="800" />
						<xsl:with-param name="height" select="600" />
					</xsl:call-template>
				</xsl:attribute>


				<xsl:call-template name="img">
					<xsl:with-param name="src" select="$image/filename"/>
					<xsl:with-param name="path" select="$image/@path"/>
					<xsl:with-param name="width" select="'200'" />
					<xsl:with-param name="height" select="'160'"/>
					<xsl:with-param name="alt" select="$image/item/title"/>
					<xsl:with-param name="mode" select="'crop-fill'"/>
				</xsl:call-template>
			</a>
		</div>

	</div>

</xsl:template>





<!--site title-->
<xsl:template match="data" mode="site-title" priority="-1">

	<!--see what the page title is-->
	<xsl:choose>
		<xsl:when test="$page-title='Home'">
			<xsl:value-of select="$site-title" />
			<xsl:text> - Homepage </xsl:text>
		</xsl:when>


		<!--otherwise nothing found -->
		<xsl:otherwise>
			<xsl:value-of select="$page-title" />
			<xsl:text> - </xsl:text>
			<xsl:value-of select="$site-title" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!--meta-->
<xsl:template match="data" mode="site-description" priority="-1">
	<xsl:value-of select="//data/settings/entry/site-description" />
</xsl:template>

<xsl:template match="data" mode="site-keywords" priority="-1">

</xsl:template>

<xsl:template match="data" mode="init-js" priority="-1"></xsl:template>


</xsl:stylesheet>
