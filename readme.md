# Symphony Boilerplate Repository

 - David Anderson
 - dave@veodesign.co.uk
 - 2014-03-19
 
 - Symphony Version 2.3.6
 
# Features

1. SASS via Compass
2. Require.js / Backbone modular JS
3. Grunt for javascript compression



# Grunt

To setup grunt, please do the following

* Edit the package.json file as needed
* Run

> sudo npm install

* To Compress the JS into app.min.js

> grunt

 
# Instructions for setup.

This repository contains all the basic symphony setup to create a new continuously integrated site.
These instructions are for the FIRST setup of the site, instructions on using this as a CI environment to follow.

1) Clone the repository

> git init
>
> git remote add boilerplate git@bitbucket.org:veodesign/new-symphony-ci-project.git
>
> git pull boilerplate master
>
> git submodules update --init

2) Install symphony by visiting public_html/install on the site

3) Install extensions (see build/src/setup_extensions.sh for a default list to install)

4) Configure the database plugin, place the site in server mode for the time being

5) Visit the preferences page and using the export ensemble tool, 'Create Install Files'

6) Move the following folders/files:
 	
1. public_html/install -> build/libs/install
 	
2. public_html/workspace/install.sql -> build/libs/install.sql

7) Ensure that there are no git file mode issues

> git config core.filemode false
>
> git submodule foreach git config core.filemode false

8) Remove the boilerplate remote and add your own remote

> git remote rm boilerplate
>
> git remote add origin git@github.com/username/your_repo
>
> git push origin master

**Congratulations, you know have a fully setup repository**

------

# Next Steps



## Setup a server install

1. SSH to the server in question and create the necessary SSH keys for github (if required)
2. Remove the public_html directory if required
3. Add and pull the repo
> git init
> git remote add origin git@github.com/username/your_repo
> git pull origin master

4. Install Script
> sh build/deploy/staging/install

5. Visit the install page to finish
6. (Optional) Setup git deploy hook and cron jon

## To use the original repo as a development repository

1. First setup the server as above and setup a user for database access
2. On the local server, visit the database configuration page and change the mode to client and input your credentials


# Appendix

##Setting up the ssh keys

Please see [this github article for setup options](https://help.github.com/articles/managing-deploy-keys). My workflow is with a github dummy account. I add the ssh keys to this and add the account as a collaborator to the account

1. SSH to your server
2. Change to the ssh key directory (cd ~/.ssh)
3. Generate a key (/usr/bin/ssh-keygen -t rsa -C "your_email@youremail.com")
4. Hit enter to use the default file and leave the passcode blank. Yes this does make things more vulnerable but it is an absolute pain otherwise. Provided the private key stays on the server and nowhere else, you'll be fine.
5. Login via ftp or similar and open your public key file (probably at .ssh/id_rsa.pub)
6. Paste this into you ssh keys on github
