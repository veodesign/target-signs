module.exports = function(grunt) {
	
	"use strict";
	
	// Configuration goes here
	grunt.initConfig({
		requirejs: {
			compile: {
				options: {
				
				
					baseUrl: "./public_html/workspace/js/",
					
				    
				    name: "app",
				    out: "./public_html/workspace/app.min.js",
					
					
					mainConfigFile: 'public_html/workspace/js/global-config.js',
					
					findNestedDependencies: true,
					preserveLicenseComments: false,

				}
			}
		}
	});
	
	// Load plugins here
	grunt.loadNpmTasks('grunt-contrib');
	
	// Define your tasks here
	grunt.registerTask('default', ['requirejs']);
	
	

};